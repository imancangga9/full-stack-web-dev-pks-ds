<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Roles extends Model
{
    protected $fillable = ['id' , 'name'];

    protected $keyType = 'string' ;

    public $incrementing = false;
    

    protected static function boot()
    {
        parent::boot();

        static::creating( function ($model){
            if(empty( $model->id) ){
                $model->id = Str::uuid();
            }
        });
    }
    
}

